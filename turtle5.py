import turtle as T

# what shape do they want?
def shape():
    print("Do you want to draw a star, a square, a triangle or a circle?")
    print("What do you want to draw?")
    shapeInput = input()



# what color do they want?
print("what color do you want your () to be?".format(shape))
colorInput = input().lower()

# how big is the shape going to be?
print("small, medium or large?")
sizeInput = input().lower()

small = 50
medium = 150
large = 200

#color, size
def star(color,size): # function for a star
    T.Screen()
    for i in range(5):
        T.color(color)
        T.forward(size)
        T.right(144)
        T.done()
def square(color,size): # function for a square
    T.Screen()
    for i in range(4):
        T.color(color)
        T.forward(size)
        T.right(90)
        T.done()
def triangle(color,size):
    T.Screen()
    for i in range(3):
        T.color(color)
        T.forward(size)
        T.right(120)
        T.done()
def circle(color,size):
    T.Screen()
    for i in range(360):
        T.color(color)
        T.forward(2)
        T.right(1)
        T.done